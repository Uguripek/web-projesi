﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Projem.Startup))]
namespace Projem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

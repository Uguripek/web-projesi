﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Projem.Models;

namespace Projem.Controllers
{
    public class EkleController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            TelefonEntities db = new TelefonEntities();
            Marka model = new Marka();
            model.Marka1 = form["Marka"].Trim();
            model.Model = form["Model"].Trim();
            db.Marka.Add(model);
            db.SaveChanges();
            return View();
        }
    }
}
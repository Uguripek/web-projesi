﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Projem.Models;
using System.Data.SqlClient;

namespace Projem.Controllers
{
    public class KarsilastiriliyorController : Controller
    {
        // GET: Karsilastiriliyor
        public ActionResult Index(int Secenek1, int Secenek2)
        {

            Models.ModelKarsilastir model = new Models.ModelKarsilastir();
            Models.TelefonEntities db = new Models.TelefonEntities();
            model.Secenek1 = db.Marka.Find(Secenek1);
            model.Secenek2 = db.Marka.Find(Secenek2);

            return View(model);
        }
    }
}